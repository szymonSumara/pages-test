# App A

This file was generated using [@jscutlery/semver](https://github.com/jscutlery/semver).

## [5.17.2] (2024-01-19)

### 🐞 Bug Fixes

- ala ma asa ([92bc1c1])

## [5.17.1] (2024-01-19)

### 🐞 Bug Fixes

- ala ma asa ([f1d107d])

## [5.17.0] (2024-01-19)

### ✨ Features

- change import ordering 123 ([2aaa4ff])

## [5.16.0] (2024-01-19)

### ✨ Features

- change import ordering 123 ([2a17c2a])
- change import ordering vvvvvv ([b6d6486])

GitHub - jscutlery/semver: Nx plugin to automate semantic versioning and CHANGELOG generation.
Nx plugin to automate semantic versioning and CHANGELOG generation. - GitHub - jscutlery/semver: Nx plugin to automate semantic versioning and CHANGELOG generation.
