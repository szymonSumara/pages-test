const showdown = require("showdown");
const fs = require("fs");

const main = async () => {
  const appsFolders = await fs.promises.readdir("./apps");
  const appsWithChangelog = [];
  for (const appName of appsFolders) {
    try {
      const fileContent = (
        await fs.promises.readFile(`./apps/${appName}/CHANGELOG.md`)
      ).toString();
      const convertedHtml = new showdown.Converter().makeHtml(fileContent);
      if (!fs.existsSync("./public")) {
        fs.mkdirSync("./public");
      }
      await fs.promises.writeFile(`./public/${appName}.html`, convertedHtml);
      appsWithChangelog.push(appName);
    } catch (err) {
      console.log(err);
    }
  }

  const index = appsWithChangelog
    .map((appName) => `<a href="./${appName}.html">${appName}</a><br \>`)
    .join("\n");
  await fs.promises.writeFile(`./public/index.html`, index);
};

main();
